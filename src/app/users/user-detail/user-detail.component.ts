import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from '../api.service';

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.scss']
})
export class UserDetailComponent implements OnInit {
  user;
  albums: any;
  posts: any;
  comments: any[];

  constructor(private route: ActivatedRoute, private router: Router, private service: ApiService) { }

  ngOnInit() {
    const userId = this.route.snapshot.paramMap.get('id');

    const promiseUser = this.service.getUserById(userId);
    const promiseAlbums = this.service.getAlbumsByUserId(userId);
    const promisePosts = this.service.getPostsByUserId(userId);

    Promise.all([promiseUser, promiseAlbums, promisePosts]).then( values => {
      this.user = values[0];
      this.getThumbnailsByAlbumId(values[1]);
      this.getCommentsByPostId(values[2]);
    });
  }

  getCommentsByPostId(posts: any) {
    posts.forEach(post => {
      this.service.getComentsByPostId(post.id).then(comments => {
          post.comments = comments;
      });
    });
    this.posts = posts;
  }

  getThumbnailsByAlbumId(albums: any) {
    albums.forEach(album => {
      this.service.getThumbnailByAlbumId(album.id).then(thumbnails => {
        album.thumbnail = thumbnails[0];
      });
    });
    this.albums = albums;
  }

}


