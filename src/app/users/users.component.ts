import { Component, OnInit } from '@angular/core';
import { ApiService } from './api.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {
  userName: string;
  allUsers;
  userList;

  constructor(private service: ApiService) { }

  ngOnInit() {
    this.service.getUsers().then(users => {
      this.allUsers = users;
      this.userList = users;
    });
  }

  searchUsers() {
    this.userList = this.allUsers.filter(user => {
      return user.name.toLowerCase().includes(this.userName.toLowerCase())
    });
  }

}
