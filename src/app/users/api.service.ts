import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  private apiUrl = environment.API_URL;

  constructor(private http: HttpClient) { }

  getUsers(): Promise<any> {
    return this.http.get(`${this.apiUrl}users`).toPromise();
  }

  getUserById(id: string): Promise<any> {
    return this.http.get(`${this.apiUrl}users/${id}`).toPromise();
  }

  getAlbumsByUserId(id: string): Promise<any> {
    return this.http.get(`${this.apiUrl}albums?userId=${id}`).toPromise();
  }

  getPostsByUserId(id: string): Promise<any> {
    return this.http.get(`${this.apiUrl}posts?userId=${id}`).toPromise();
  }

  getComentsByPostId(id: string): Promise<any> {
    return this.http.get(`${this.apiUrl}comments?postId=${id}`).toPromise();
  }

  getThumbnailByAlbumId(id: string): Promise<any> {
    return this.http.get(`${this.apiUrl}photos?albumId=${id}`).toPromise();
  }



}
